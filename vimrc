" macOS version
set nocompatible              " be iMproved, required
set rtp+=/usr/local/opt/fzf
call plug#begin('~/.vim/plugged')

Plug 'junegunn/vim-easy-align'

" tmux
Plug 'christoomey/vim-tmux-navigator'
Plug 'tmux-plugins/vim-tmux'
Plug 'benmills/vimux'

" Coc
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'tpope/vim-fugitive'

" fzf
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'

" Ack search tool
Plug 'mileszs/ack.vim'

" Indentation
Plug 'Yggdroot/indentLine'

" Python autoformat
Plug 'psf/black'

" Django HTML
Plug 'tweekmonster/django-plus.vim'

" comment
Plug 'scrooloose/nerdcommenter'

Plug 'tpope/vim-sensible'
Plug 'tpope/vim-sleuth'

" gitgutter
Plug 'airblade/vim-gitgutter'

" emmet
Plug 'mattn/emmet-vim'

" Ale
Plug 'w0rp/ale'

" lightline
Plug 'itchyny/lightline.vim'

" Eslint
Plug 'vim-syntastic/syntastic'

" EcmaScript and JSX
Plug 'pangloss/vim-javascript'
Plug 'maxmellon/vim-jsx-pretty'

" TypeScript
Plug 'leafgarland/typescript-vim'
Plug 'Quramy/tsuquyomi'
Plug 'Shougo/vimproc.vim'

" Lua
Plug 'xolox/vim-misc'
Plug 'xolox/vim-lua-ftplugin'

" Go format
Plug 'fatih/vim-go'

" Rust format
" Plug 'rust-lang/rust.vim'

" Ruby format
" Plug 'ruby-formatter/rufo-vim'

" NERDTree
Plug 'scrooloose/nerdtree' ", {'on': 'NERDTreeToggle'}
Plug 'Xuyuanp/nerdtree-git-plugin'

" Colorscheme
Plug 'morhetz/gruvbox'
Plug 'nanotech/jellybeans.vim'
" markdown
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'branch': 'release/1.x',
  \ 'for': [
    \ 'javascript',
    \ 'typescript',
    \ 'css',
    \ 'less',
    \ 'scss',
    \ 'json',
    \ 'graphql',
    \ 'markdown',
    \ 'vue',
    \ 'lua',
    \ 'swift' ] }

" plugin from http://vim-scripts.org/vim/scripts.html
" Git plugin not hosted on GitHub
Plug 'git://git.wincent.com/command-t.git'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plug 'rstacruz/sparkup', {'rtp': 'vim/'}

" All of your Plugins must be added before the following line
call plug#end()            " required
" To ignore plugin indent changes, instead use:
" Put your non-Plugin stuff after this line

" Find files
set path+=**
set wildmenu

set mouse=a
set hidden
set cmdheight=2
set nobackup
set nowritebackup
" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes
" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr> <TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr> <S-TAB> pumvisible() ? "\<C-p>" : "\<S-TAB>"

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

set updatetime=100
set timeout timeoutlen=3000 ttimeoutlen=100
set number
set tabstop=4
set smarttab
set softtabstop=4
set expandtab
set textwidth=80
set colorcolumn=81
set wrapmargin=0
set formatoptions=croqlt12
set wrap
set shiftwidth=2
set autoindent
set smartindent
set showcmd
set cursorline
set guicursor+=i:block-Cursor
set lazyredraw
set showmatch
set incsearch
set hlsearch
set foldenable
set cindent
set shell=/bin/bash
set viminfo='100,<1000,s100,h
colorscheme gruvbox
set background=dark
let g:gruvbox_contrast_dark='hard'
set foldlevelstart=99
set foldnestmax=10
nnoremap <space> za

" new line without insert mode
nmap <S-Enter> O<Esc>j
nmap <CR> o<Esc>k

set foldmethod=indent
inoremap ( ()<Esc>i
inoremap (<CR> (<CR>)<C-o>O
inoremap [ []<Esc>i
inoremap [<CR> [<CR>]<C-o>O
"inoremap < <><Esc>i
inoremap { {}<Esc>i
inoremap ` ``<Esc>i
inoremap {<CR> {<CR>}<C-o>O
inoremap `<CR> `<CR>`<C-o>O
"inoremap <C-Return> <CR><CR><C-o>k<Tab>
inoremap " ""<Esc>i
inoremap ' ''<Esc>i

" navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow
set splitright

" fugitive
autocmd QuickFixCmdPost *grep* cwindow
set diffopt+=vertical

" Vimux
" Prompt for a command to run
map <Leader>vp :VimuxPromptCommand<CR>
" Run last command executed by VimuxRunCommand
map <Leader>vl :VimuxRunLastCommand<CR>

" Coc config
set completeopt+=preview
set completeopt+=menuone
set completeopt+=noselect
autocmd CursorHold * silent call CocActionAsync('highlight')

autocmd FileType json syntax match Comment +\/\/.\+$+
let g:ale_linters = {
    \ 'sh': ['language_server'],
    \ }
autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')

" gitgutter
let g:gitgutter_max_signs = 999

" NERD Commenter
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1

function! Formatonsave()
  let l:formatdiff = 1
  pyf ~/clang-format.py
endfunction

" lightline colorscheme
function! CocCurrentFunction()
    return get(b:, 'coc_current_function', '')
endfunction

let g:lightline = {
      \ 'colorscheme': 'one',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'cocstatus', 'currentfunction', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'cocstatus': 'coc#status',
      \   'currentfunction': 'CocCurrentFunction'
      \ },
      \ }
" fzf search
" Default fzf layout
" - down / up / left / right
let g:fzf_layout = { 'down': '~30%' }
" In Neovim, you can set up fzf window using a Vim command
" let g:fzf_layout = { 'window': 'enew' }
" let g:fzf_layout = { 'window': '-tabnew' }
" let g:fzf_layout = { 'window': '10split' }

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-i': 'split',
  \ 'ctrl-s': 'vsplit' }

autocmd!  FileType fzf set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler

command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --hidden --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)

nnoremap <C-g> :Rg<Cr>

" Global line completion (not just open buffers. ripgrep required.)
inoremap <expr> <c-x><c-l> fzf#vim#complete(fzf#wrap({
  \ 'prefix': '^.*$',
  \ 'source': 'rg -n ^ --color always',
  \ 'options': '--ansi --delimiter : --nth 3..',
  \ 'reducer': { lines -> join(split(lines[0], ':\zs')[2:], '') }}))

" [Buffers] Jump to the existing window if possible
" let g:fzf_buffers_jump = 1

nnoremap <silent> <Leader>s :call fzf#run({
\   'down': '40%',
\   'sink': 'botright split' })<CR>

" Open files in vertical horizontal split
nnoremap <silent> <Leader>v :call fzf#run({
\   'right': winwidth('.') / 2,
\   'sink':  'vertical botright split' })<CR>

let g:ackprg = 'rg --vimgrep --no-heading'
cnoreabbrev ag Ack
cnoreabbrev aG Ack
cnoreabbrev Ag Ack
cnoreabbrev AG Ack

let g:vim_jsx_pretty_colorful_config = 1
let g:typescript_indent_disable = 0
let g:typescript_ignore_browserwords = 0

autocmd BufWritePre *.h,*.cc,*.c,*.cpp call Formatonsave()
map <C-K> :pyf ~/clang-format.py<cr>
imap <C-K> <c-o>:pyf ~/clang-format.py<cr>
" let g:rustfmt_autosave=1
let g:autopep8_aggressive=1
let g:autopep8_disable_show_diff=1
let g:autopep8_on_save=1
let g:autopep8_max_line_length=120

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%{coc#status()}
set statusline+=%*

let g:syntastic_python_checkers = ['pylint']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exe = 'npm run lint --'
let g:elm_syntastic_show_warnings = 1

" NERDTree
let g:NERDTreeWinSize = 27
" autocmd vimenter * NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
map <C-n> :NERDTreeToggle<CR>

" start vim/nvim with $vim or $nvim, NOT $vim . or $nvim .
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

" Please customize this:
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }

let g:prettier#config#print_width = 80
let g:prettier#config#html_whitespace_sensitivity = 'css'
let g:prettier#config#trailing_comma = 'all'
let g:prettier#config#semi = 'true'
let g:prettier#autoformat = 0
" autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml PrettierAsync
autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml Prettier

" Auto-format Go
autocmd BufWritePre *.go :call CocAction('runCommand', 'editor.action.organizeImport')

" Python Black
let g:black_linelength = 120
let g:black_skip_string_normalization = 1
" Auto run Black
autocmd BufWritePre *.py execute ':Black'
" djangohtml
au BufNewFile,BufRead *.html set filetype=htmldjango
