# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH=/usr/local/sbin:$PATH
export GOPATH=$HOME/go
export PATH=${GOPATH//://bin:}/bin:$PATH
export TERM=xterm-256color
# Path to your oh-my-zsh installation.
export ZSH="/Users/gs069673/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"
# ZSH_THEME="agnoster"
# ZSH_THEME=powerlevel10k/powerlevel10k

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"
DISABLE_UPDATE_PROMPT=true

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=1

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git 
  zsh-nvm
)

[ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh

source $ZSH/oh-my-zsh.sh
source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# User configuration
# DEFAULT_USER=gs069673@C02YC1XFJGH5
prompt_context() {}
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(ssh dir vcs nvm)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=()
# NODE_VIRTUAL_ENV_DISABLE_PROMPT=1
# export MANPATH="/usr/local/man:$MANPATH"

alias ls='lsd'

# autoload -Uz vcs_info
# setopt PROMPT_SUBST
# zstyle ':vcs_info:*' enable git svn cvs hg
# zstyle ':vcs_info:*' get-revision true

### Display the existence of files not yet known to VCS

### git: Show marker (T) if there are untracked files in repository
# Make sure you have added staged to your 'formats':  %c
# zstyle ':vcs_info:git*+set-message:*' hooks git-untracked
#
# zstyle ':vcs_info:*' check-for-changes true
# zstyle ':vcs_info:*' check-for-staged-changes true
# zstyle ':vcs_info:*' stagedstr '%{%F{green}%B%}✚%{%b%f%}'
# zstyle ':vcs_info:*' unstagedstr '%{%F{red}%B%}●%{%b%f%}'
# +vi-git-untracked() {
#   if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
#      git status --porcelain | grep -m 1 '^??' &>/dev/null
#   then
#     hook_com[misc]='%{'${fg[red]}'%}?'
#   fi
# }
#
# zstyle ':vcs_info:*' actionformats '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%{'${fg[red]}'%}%b%F{3}|%F{1}%a%F{5}]%f '
# zstyle ':vcs_info:*' formats '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%{'${fg[red]}'%}%b%F{5}]%f '
# zstyle ':vcs_info:(git):*' actionformats '%F{5}[%F{2}%{'${fg[red]}'%}%b%F{3}|%F{1}%a%F{5}](%m%u%c%F{5})%f '
# zstyle ':vcs_info:(git):*' formats '%F{5}[%F{2}%{'${fg[red]}'%}%b%F{5}](%m%u%c%F{5})%f '
# zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'
#
# precmd() {
#     vcs_info
# }
#
# vcs_info_wrapper() {
#   vcs_info
#   if [ -n "$vcs_info_msg_0_" ]; then
#     echo "${vcs_info_msg_0_}%{$reset_color%}$del"
#   fi
#   # Displaying upstream dedicated segment
#   if [[ -n $remote ]]; then
#         if [[ $behind -ne 0 ]]; then
#           prompt_segment magenta white
#         else
#           prompt_segment cyan black
#         fi
#         echo -n " $remote (-$behind)"
#   fi
# }

# function get_git_status() {
#   if [[ -n $(git rev-parse --is-inside-work-tree 2>/dev/null) ]]; then
#     local git_status="$(git_prompt_status)"
#     if [[ -n $git_status ]]; then
#       git_status="($git_status%{$reset_color%})%{$fg[cyan]%}] "
#     fi
#     echo $git_status
#   fi
# }

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[cyan]%}[%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY=" %{$fg[yellow]%}✗%{$fg[cyan]%}]"
ZSH_THEME_GIT_PROMPT_CLEAN=" %{$fg[green]%}✓%{$fg[cyan]%}]"

# Git status
ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%}+%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}-%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[magenta]%}*%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%}>%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[cyan]%}=%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[yellow]%}?%{$reset_color%}"

local LAMBDA="%(?,%{$fg_bold[green]%}λ,%{$fg_bold[red]%}λ)"
local ARROW="%(?,%{$fg_bold[green]%}➜,%{$fg_bold[red]%}➜)"
export PROMPT='$LAMBDA %{$fg[cyan]%}%c%{$reset_color%} $(git_prompt_info)$ARROW %{$reset_color%}'
# export PROMPT='$LAMBDA %{$fg[cyan]%}%c%{$reset_color%} $(vcs_info_wrapper)${ret_status} %{$reset_color%}'

# color for Terminal.app
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

# fzf search
export FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden --follow --glob "!.git/*"'

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

alias golon="cd ~/git-repos/lon && source docker/dev/activate.sh"


export YVM_DIR=/usr/local/opt/yvm
[ -r $YVM_DIR/yvm.sh ] && . $YVM_DIR/yvm.sh
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
export XDG_CONFIG_HOME="$HOME/dotfiles"
export MYVIMRC="$HOME/dotfiles/vimrc"
export VIMINIT="source $MYVIMRC"
