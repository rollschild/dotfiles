set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
set guicursor=
let g:typescript_ignore_browserwords = 0
source ~/dotfiles/vimrc
